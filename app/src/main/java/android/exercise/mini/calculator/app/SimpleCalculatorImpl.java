package android.exercise.mini.calculator.app;

import java.io.Serializable;

public class SimpleCalculatorImpl implements SimpleCalculator {

  // todo: add fields as needed
  private String output = "0";
  private int leftSide = 0;
  private int rightSide = 0;
  private boolean isPlus = true;
  private int length = 0;

  private void tempFinishing(){
    if(isPlus){
      leftSide += rightSide;
    }
    else{
      leftSide -= rightSide;
    }
    rightSide = 0;
  }

  @Override
  public String output() {
    // todo: return output based on the current state
    return output;
  }

  @Override
  public void insertDigit(int digit) {
    // todo: insert a digit
    //rightSide = rightSide*10+digit;
    if(digit > 9 || digit < 0){
      throw new IllegalArgumentException();
    }
    if(length == 0){
      output = "";
    }
     output += digit;
    length++;
  }

  @Override
  public void insertPlus() {
    // todo: insert a plus
   /* if(output.charAt(output.length() - 1) == '+' || output.charAt(output.length() - 1) == '-'){
      return;
    }
    tempFinishing(); */
    output += "+";
    length++;
 //   isPlus = true;
  }

  @Override
  public void insertMinus() {
    // todo: insert a minus
 /*   if(output.charAt(output.length() - 1) == '+' || output.charAt(output.length() - 1) == '-'){
      return;
    }
    tempFinishing(); */
    output += "-";
    length++;
 //   isPlus = false;
  }

  @Override
  public void insertEquals() {
    // todo: calculate the equation. after calling `insertEquals()`, the output should be the result
    //  e.g. given input "14+3", calling `insertEquals()`, and calling `output()`, output should be "17"
    /*tempFinishing();
    output = String.valueOf(leftSide);*/
    for (int i = 0; i<output.length(); ++i){
      char curr = output.charAt(i);
      if(curr == '+'){
        tempFinishing();
        isPlus = true;
      }
      else if(curr == '-'){
        tempFinishing();
        isPlus = false;
      }
      else{
        rightSide = rightSide*10 + Character.getNumericValue(curr);
      }
    }
    tempFinishing();
    output = String.valueOf(leftSide);
    length = output.length();
    leftSide = 0;
    isPlus = true;
  }

  @Override
  public void deleteLast() {
    // todo: delete the last input (digit, plus or minus)
    //  e.g.
    //  if input was "12+3" and called `deleteLast()`, then delete the "3"
    //  if input was "12+" and called `deleteLast()`, then delete the "+"
    //  if no input was given, then there is nothing to do here
    if(length == 0){
      return;
    }
    output = output.substring(0, output.length() -1);
    length--;
  }

  @Override
  public void clear() {
    // todo: clear everything (same as no-input was never given)
    output = "0";
    leftSide = 0;
    rightSide = 0;
    isPlus = true;
    length = 0;
  }

  @Override
  public Serializable saveState() {
    CalculatorState state = new CalculatorState();
    // todo: insert all data to the state, so in the future we can load from this state
    state.isPlus = this.isPlus;
    state.leftSide = this.leftSide;
    state.rightSide = this.rightSide;
    state.output = this.output;
    state.length = this.length;
    return state;
  }

  @Override
  public void loadState(Serializable prevState) {
    if (!(prevState instanceof CalculatorState)) {
      return; // ignore
    }
    CalculatorState casted = (CalculatorState) prevState;
    // todo: use the CalculatorState to load
    this.output = casted.output;
    this.leftSide = casted.leftSide;
    this.rightSide = casted.rightSide;
    this.isPlus = casted.isPlus;
    this.length = casted.length;
  }

  private static class CalculatorState implements Serializable {
    /*
    TODO: add fields to this class that will store the calculator state
    all fields must only be from the types:
    - primitives (e.g. int, boolean, etc)
    - String
    - ArrayList<> where the type is a primitive or a String
    - HashMap<> where the types are primitives or a String
     */
     public String output = "0";
     public int leftSide = 0;
     public int rightSide = 0;
     public boolean isPlus = true;
     public int length = 0;
  }
}
