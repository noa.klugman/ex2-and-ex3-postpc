# AndroidCalculator - Calculator exercise for Android developers

**I promise I have written the code alone**

**Hypothetical question - ex3:**
We need to add a multipication function to support this feature.
We can run tests of:
- checking that the button works and it sends to the fitting function in the calculator implamentation
- flow test - check if it does the right operation and calculation.

## In this project:
- Calculator screen with XML ready for portrait and landscape
- Calculator interface used by the Activity
- Unit tests for the calculator and the activity

## Your job:
- Implement `SimpleCalculatorImpl.java`
- add more unit tests to `SimpleCalculatorImpl.java`
- Implement `MainActivity.java`
- add more unit tests to `MainActivityTest.java`
- add more flow tests to `AppFlowTest.java`

Basically look for "TODO" in the code.


Good luck!
